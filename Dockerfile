# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM alpine:3.14

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates \
    jq \
    curl \
    bash \
    openssl \
    openssl-dev \
    grep \
    bc \
    wget \
    util-linux \
    uuidgen \
    bash \
    coreutils \
    ca-certificates

# Install kubectl
ARG KUBECTL_VERSION=v1.21.1
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$KUBECTL_VERSION/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl

# Disable currently fetch always latest version
# Current Error:
# awk: bad regex '[0-9]+.[0-9]+.[0-9]+[.\-A-Za-z0-9]*': Invalid character range
#ARG KEPTN_VERSION=0.8.2
# Install keptn-cli
#RUN curl -sL https://get.keptn.sh | bash
ARG KEPTN_VERSION=0.9.2
RUN wget https://github.com/keptn/keptn/releases/download/$KEPTN_VERSION/keptn-$KEPTN_VERSION-linux-amd64.tar.gz && \
   tar xvf keptn-$KEPTN_VERSION-linux-amd64.tar.gz && \
   mv keptn-$KEPTN_VERSION-linux-amd64 /usr/local/bin/keptn

# Install yq
ARG YQ_VERSION=v4.9.0
ARG YQ_BINARY=yq_linux_amd64
RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/${YQ_BINARY}.tar.gz -O - |\
  tar xz && mv ${YQ_BINARY} /usr/bin/yq

ARG HELM_VERSION="v3.7.0"
RUN curl -L https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar -xz --strip-components=1 linux-amd64/helm && mv helm /usr/local/bin/helm && chmod +x /usr/local/bin/helm
RUN set -ex; helm version

COPY scripts/ /usr/bin/
